import Vue from 'vue'
import Navigation from './components/Navigation'
import Vuetify from 'Vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueRouter from 'vue-router'
import axios from 'axios'
import io from 'socket.io-client'
import Vuex from 'vuex'

Vue.use(Vuex)
Vue.config.devtools = true
Vue.use(Vuetify);
Vue.use(VueRouter)

/*
* import router components
* */
import Kitchen from './views/Kitchen'
import Persons from './views/Persons'
import Dishes from './views/Dishes'
import Orders from './views/Orders'

const routes = [
    {
        path: '/',
        component: Kitchen
    },
    {
        path: '/persons',
        component: Persons
    },
    {
        path: '/dishes',
        component: Dishes
    },
    {
        path: '/orders',
        component: Orders
    }
]


const router = new VueRouter({
    routes // short for `routes: routes`
})

const store = new Vuex.Store({
    state: {
        socket: io.connect('http://localhost:3000')
    },
    getters:{
        getSocket: state => {
            return state.socket
        }
    }
})

const application = new Vue({
    el: '#app',
    components: {
        Navigation
    },
    data: {
        message: 'Hallo mein Freund!'
    },
    router,
    store
})