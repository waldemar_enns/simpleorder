<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Person extends Model
{
    public $table = "persons";
    protected $fillable = ['name'];

    public function orders(){
        return $this->hasMany(Order::class);
    }
}
